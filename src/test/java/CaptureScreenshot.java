import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import unit.LoginTest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureScreenshot extends LoginTest {
    public void capture(){
        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        String filename =  new SimpleDateFormat("yyyyMMddhhmmss'.jpeg'").format(new Date());
        File DestFile=new File("./src/test/TestScreenShots/"+filename);
        try{
            FileUtils.copyFile(SrcFile, DestFile);}
        catch (Exception e){
            System.out.println(e.toString());
        }
        System.out.println("Screenshot Captured");
    }
}
