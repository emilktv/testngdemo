package unit;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.File;

public class LoginTest {
    public static WebDriver driver;
    //Before method to setup webdriver instance
    @BeforeMethod
    public void setup(){
        String browserName=System.getProperty("browser");
        if(browserName.toLowerCase().equals("chrome")){
            WebDriverManager.chromedriver().setup();
            ChromeOptions options=new ChromeOptions();
            options.setHeadless(true);
            driver=new ChromeDriver(options);
        }
        else if(browserName.toLowerCase().equals("firefox")){
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(true);
            driver=new FirefoxDriver(options);
        }
        driver.get("http://practice.automationtesting.in/my-account/");
    }
    //Login Test
    @Test(dataProvider="loginValidInputs",dataProviderClass=DataProviders.class)
    public void loginTestValidInputs(String username,String password){
        WebElement loginGreetText;
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.name("login")).click();
        loginGreetText=driver.findElement(By.className("woocommerce-MyAccount-content"));
        Assert.assertTrue(loginGreetText.getText().contains("Hello"));
    }
    //Invalid Input Login test
    @Test(dataProvider="loginValidInputs",dataProviderClass=DataProviders.class)
    public void loginTestInvalidInputs(String username,String password){
        WebElement loginGreetText;
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.name("login")).click();
        loginGreetText=driver.findElement(By.className("woocommerce-MyAccount-content"));
        Assert.assertTrue(loginGreetText.getText().contains("Hello"));
    }
    //After method to close driver
    @AfterMethod
    public void tearDown(){
        driver.close();
    }
}
