package unit;

import org.testng.annotations.DataProvider;

public class DataProviders {
    //DataProviders for valid and Invalid Inputs
    @DataProvider(name="loginValidInputs")
    public static Object[][] loginValidInputs(){
        return new Object[][]{
                {"a@g.in","RegisterPassword11235813!!mypass"},
                {"a@g.in","RegisterPassword11235813!!mypass"}
        };
    }
    @DataProvider(name="loginInvaildInputs")
    public static Object[][] loginInvalidInputs(){
        return new Object[][]{
                {"invalidEmail","RegisterPassword11235813!!mypass"},
                {"invalidID","RegisterPassword11235813!!mypass"},
                {"a@g.in","invalidPassword"},
                {"a10","invalidPassword"},
                {"invalidEmail","invalidPassword"},
                {"invalidID","invalidPassword"},
                {"",""}
        };
    }
}
